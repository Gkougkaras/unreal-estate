const   express = require("express"),
        router =express.Router(),
        PropertyRent = require("../models/property_rent"),
        middleware = require("../middleware"),
        NodeGeocoder = require('node-geocoder');
 
const options = {
  provider: 'google',
  httpAdapter: 'https',
  apiKey: process.env.GEOCODER_API_KEY,
  formatter: null
};
 
const geocoder = NodeGeocoder(options);


//INDEX ROUTE

router.get("/",function(req, res){
    if(req.query.search){
        const regex = new RegExp(middleware.regex(req.query.search), "gi");
        PropertyRent.find({$or: [{location: regex}, {size:regex}, {price:regex}]},function(err, foundPropertyRent){
            if (err){
                res.flash("error", "Something went wrong please try again");
                return res.redirect('back');
            }else{
                res.render("properties_rent/index", {properties_rent: foundPropertyRent});
            }
        });
    }else{
    PropertyRent.find({},function(err, foundPropertyRent){
        if (err){
            res.flash("error", "Something went wrong please try again");
            return res.redirect('back');
        }else{
            res.render("properties_rent/index", {properties_rent: foundPropertyRent});
        }
});
    }
});


//POST ROUTE

router.post("/", middleware.isLoggedIn, function(req, res){
    const description   = req.body.description;
    const bedroom      = req.body.bedroom;
    const image         = req.body.image;
    const price         = req.body.price;
    const size          = req.body.size;
     const owner = {
         id:req.user._id,
         username:req.user.username
     }
     geocoder.geocode(req.body.location, function (err, data) {
        if (err || !data.length) {
          req.flash('error', 'Invalid address');
          return res.redirect('back');
        }
        var lat = data[0].latitude;
        var lng = data[0].longitude;
        var location = data[0].formattedAddress;
    const newPropertyRent = {location: location, lat: lat, lng: lng,image:image, description:description, price: price, owner: owner, size:size, bedroom:bedroom};
    PropertyRent.create(newPropertyRent,function(err, newlyCreated){
        if(err){
            res.flash("error", "Sorry we could not create the ad, please try again");
            return res.redirect('back');
        }else{
            res.redirect("/properties_rent");
        }
        });
    });
});

//NEW ROUTE

router.get("/new",middleware.isLoggedIn, function(req, res){
    res.render("properties_rent/new");
});

//SHOW ROUTE

router.get("/:id",function(req, res){
    PropertyRent.findById(req.params.id).populate("comments").exec(function(err,foundPropertyRent){
        if (err || !foundPropertyRent){
            req.flash("error", "Could not load Properties please try again");
            return res.redirect("/properties_rent");
        }
        else{
            res.render("properties_rent/show", {property_rent: foundPropertyRent});
        }
    });
});

//EDIT ROUTE
router.get("/:id/edit",middleware.checkPropertyRentOwnership,function(req, res){
    PropertyRent.findById(req.params.id, function(err,foundPropertyRent){
         res.render("properties_rent/edit", {property_rent: foundPropertyRent});
    });
});

//UPDATE ROUTE
router.put("/:id",middleware.checkPropertyRentOwnership,function(req,res){
    geocoder.geocode(req.body.location, function (err, data) {
        if (err || !data.length) {
          req.flash('error', 'Invalid address');
          return res.redirect('back');
        }
        req.body.property_rent.lat = data[0].latitude;
        req.body.property_rent.lng = data[0].longitude;
        req.body.property_rent.location = data[0].formattedAddress;
    PropertyRent.findByIdAndUpdate(req.params.id,req.body.property_rent,function(err,updatedPropertyRent){
        if (err){
            req.flash("error", "Could not update the property please try again")
            res.redirect("/properties_rent");
        }else{
            res.redirect("/properties_rent/"+ req.params.id);
        }
    });
    });
});

//DESTROY ROUTE

router.delete("/:id",middleware.checkPropertyRentOwnership,function(req, res){
    PropertyRent.findByIdAndRemove(req.params.id, function(err){
        if (err){
            req.flash("error", "Could not delete the proprty please try again")
            res.redirect("/properties_rent");
        }else{
            res.redirect("/properties_rent");
        }
    });
});

module.exports = router;