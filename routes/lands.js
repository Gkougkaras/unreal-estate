const       express         = require("express"),
            router          = express.Router(),
            Land            = require("../models/land"),
            middleware      = require("../middleware"),
            NodeGeocoder    = require('node-geocoder');
 
const options = {
  provider: 'google',
  httpAdapter: 'https',
  apiKey: process.env.GEOCODER_API_KEY,
  formatter: null
};
 
const geocoder = NodeGeocoder(options);


//INDEX ROUTE

router.get("/",function(req, res){
    if(req.query.search){
        const regex = new RegExp(middleware.regex(req.query.search), "gi");
        Land.find({$or: [{location: regex}, {size:regex}, {price:regex}]},function(err, foundLand){
            if (err){
                res.flash("error", "Something went wrong please try again");
                return res.redirect('back');
            }else{
                res.render("lands/index", {lands: foundLand});
            }
        });
    }else{
    Land.find({},function(err, foundLand){
        if (err){
            res.flash("error", "Something went wrong please try again");
            return res.redirect('back');
        }else{
            res.render("lands/index", {lands: foundLand});
        }
    });
    }
});

//POST ROUTE

router.post("/", middleware.isLoggedIn,function(req, res){
    const description   = req.body.description;
    const image         = req.body.image;
    const price         = req.body.price;
    const size          = req.body.size;
    const owner = {
        id: req.user._id,
        username: req.user.username
    }
    geocoder.geocode(req.body.location, function (err, data) {
        if (err || !data.length) {
          req.flash('error', 'Invalid address');
          return res.redirect('back');
        }
        var lat = data[0].latitude;
        var lng = data[0].longitude;
        var location = data[0].formattedAddress;
    const newLand = {location: location, lat: lat, lng: lng,image:image, description:description,price: price, owner: owner, size:size};
    Land.create(newLand,function(err, newlyCreated){
        if(err){
            req.flash("error", "Sorry we could not create the ad, please try again");
            return res.redirect('back');
        }else{
            res.redirect("/lands");
        }
    });
    });
});

//NEW ROUTE

router.get("/new",middleware.isLoggedIn, function(req, res){
    res.render("lands/new");
});

//SHOW ROUTE

router.get("/:id", function(req, res){
    Land.findById(req.params.id, function(err, foundLand){
        if(err || !foundLand){
            req.flash("error", "Could not load Properties please try again");
            return res.redirect("/lands");
        }else{
            res.render("lands/show", {land: foundLand})
        }
    });
});

//EDIT ROUTE
router.get("/:id/edit",middleware.checkLandOwnership,function(req, res){
    Land.findById(req.params.id, function(err,foundLand){
         res.render("lands/edit", {land: foundLand});
    });
});

//UPDATE ROUTE
router.put("/:id",middleware.checkLandOwnership,function(req,res){
    geocoder.geocode(req.body.location, function (err, data) {
        if (err || !data.length) {
          req.flash('error', 'Invalid address');
          return res.redirect('back');
        }
        req.body.land.lat = data[0].latitude;
        req.body.land.lng = data[0].longitude;
        req.body.land.location = data[0].formattedAddress;    
    Land.findByIdAndUpdate(req.params.id,req.body.land,function(err,updatedLand){
        if (err){
            req.flash("error", "Could not update the proprty please try again")
            res.redirect("/lands");
        }else{
            res.redirect("/lands/"+ req.params.id);
        }
    });
    });
});

//DESTROY ROUTE

router.delete("/:id",middleware.checkLandOwnership,function(req, res){
    Land.findByIdAndRemove(req.params.id, function(err){
        if (err){
            req.flash("error", "Could not delete the proprty please try again")
            res.redirect("/lands");
        }else{
            res.redirect("/lands");
        }
    });
});

module.exports = router;