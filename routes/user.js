const   express         = require("express"),
        router          = express.Router(),
        User            = require("../models/user"),
        middleware      = require("../middleware"),
        Property        = require("../models/property"),
        PropertyRent    = require("../models/property_rent"),
        Land            = require("../models/land");

//SHOW ROUTE

router.get("/:id", function(req,res){
    User.findById(req.params.id, function(err, foundUser){
        if(err){
            req.flash("error", "User Profile not found");
            res.redirect("/");
        }else
        Property.find().where("owner.id").equals(foundUser._id).exec(function(err, properties){
            if (err){
                req.flash("error", "Property not found");
                res.redirect("/");
            }
        PropertyRent.find().where("owner.id").equals(foundUser._id).exec(function(err, properties_rent){
            if (err){
                req.flash("error", "Property not found");
                res.redirect("/");
            }
        Land.find().where("owner.id").equals(foundUser._id).exec(function(err, lands){
            if (err){
                req.flash("error", "Property not found");
                res.redirect("/");
            }
        res.render("users/show",{user: foundUser, properties: properties,properties_rent:properties_rent, lands:lands});
        });
        });
        });
    });
});

//EDIT ROUTE

router.get("/:id/edit",function(req, res){
    User.findById(req.params.id, function(err,foundUser){
         res.render("users/edit", {user: foundUser});
    });
});

//UPDATE ROUTE
router.put("/:id",function(req,res){
    User.findByIdAndUpdate(req.params.id,req.body.user,function(err,updatedUser){
        if (err){
            req.flash("error","Profile not updated, please try again");
            res.redirect("/login");
        }else{
            res.redirect("/users/"+ req.params.id);
        }
    });
    });


module.exports = router;