const   express         = require("express"),
        router          = express.Router({mergeParams:true}),
        PropertyRent    = require("../models/property_rent"),
        Comment         = require("../models/comment"),
        middleware      = require("../middleware");

//COMMENTS NEW

router.get("/new", middleware.isLoggedIn, function(req, res){
    PropertyRent.findById(req.params.id, function(err, propertyRent){
        if(err){
            console.log(err);
        }
        else{
            res.render("comments/new", {propertyRent: propertyRent});
        }
    });
});

//COMMENTS CREATE

router.post("/",middleware.isLoggedIn, function(req, res){
    PropertyRent.findById(req.params.id,function(err, propertyRent){
        if(err){
            req.flash("error","Comment was not created");
            res.redirect("/properties_rent");
        }
        else{
            Comment.create(req.body.comment, function(err,comment){
                if(err){
                    req.flash("error", "Oops, Something Went Wrong!!");
                    console.log(err);
                }
                else{
                    comment.owner.id = req.user._id;
                    comment.owner.username = req.user.username;
                    comment.save();
                    propertyRent.comments.push(comment);
                    propertyRent.save();
                    req.flash("success", "Comment successfully added!!");
                    res.redirect("/properties_rent/" + propertyRent._id);
                }
            });
         }
        });
});

//EDIT COMMENTS
router.get("/:comment_id/edit",middleware.checkCommentOwnership,function(req, res){
    PropertyRent.findById(req.params.id,function(err,foundPropertyRent){
        if(err || !foundPropertyRent){
            req.flash("error","Property not found");
            return res.redirect("back");
        }
        Comment.findById(req.params.comment_id,function(err,foundComment){
            if(err || !foundComment){
                req.flash("error","Comment not found");
                res.redirect("back");
            }else{
                res.render("comments/edit",{propertyRent_id: req.params.id,comment: foundComment});
            }
        });
    }); 
});

//UPDATE COMMENTS

router.put("/:comment_id",middleware.checkCommentOwnership,function(req,res){
    Comment.findByIdAndUpdate(req.params.comment_id,req.body.comment,function(err,updatedComment){
        if (err){
            res.redirect("back");
        }else{
            req.flash("success", "Comment Succesfully updated");
            res.redirect("/properties_rent/"+ req.params.id);
        }
    });
});

//DESTROY COMMENTS

router.delete("/:comment_id",middleware.checkCommentOwnership,function(req, res){
    Comment.findByIdAndRemove(req.params.comment_id,function(err){
        if (err){
            res.redirect("back");
        }else{
            req.flash("success", "Comment Succesfully deleted");
            res.redirect("/properties_rent/"+ req.params.id);
        }
    });
});


module.exports = router;