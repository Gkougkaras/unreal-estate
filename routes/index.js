const  express       = require("express"),
       router        = express.Router(),
       passport      = require("passport"),
       User          = require("../models/user"),
       middleware 	 = require("../middleware"),
       async 		 = require("async"),
       nodemailer 	 = require("nodemailer"),
       crypto 		 = require("crypto");

//SETTING sgMAIL 
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);


//ROOT ROUTE

router.get("/", function(req, res){
    res.render("home");
});

// ABOUT US ROUTE

router.get("/aboutus",function(req, res){
    res.render("aboutus");
});

// CAREERS ROUTE

router.get("/careers",function(req, res){
    res.render("careers");
});

//CONTACT FORM ROUTE

//INDEX ROUTE
router.get("/contact", middleware.isLoggedIn, (req, res) => {
    res.render("contact");
});

//POST ROUTE
router.post("/contact", async (req, res) =>{
    const msg = {
        to: 'gougarasv@gmail.com',
        from: req.body.email,
        subject: 'Unreal Estate support',
        text: req.body.message,
        html: req.body.message,
      };
      try {
          await sgMail.send(msg);
          req.flash("success", "Thank you for your email, we will get back shortly");
          res.redirect("/contact");
      }catch (error){
        console.error(error);

       if (error.responce){
           console.error(error.responce.body)
           console.log(err.response.body)
       }
       req.flash("error", "Sorry something went wrong, please contact us at email BG510020@gmail.com");
       res.redirect("back");
      }
});

//SHOW LOGIN FORM

router.get("/login", function(req, res){
    res.render("login");
});

//LOGIN LOGIC ROUTE
router.post("/login", passport.authenticate("local",{
    successRedirect:"/",
    failureRedirect:"/login"}),
    function(req,res){
    });

//SHOW SIGN UP FORM 

router.get("/signup",function(req, res){
    res.render("signup");
});

//SIGN UP LOGIC ROUTE
router.post("/signup",function(req,res){
    const newUser = new User({
		username: req.body.username,
		email: req.body.email,
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		address:req.body.address,
		city: req.body.city,
		avatar: req.body.avatar,
		phone: req.body.phone});
    if (req.body.adminCode === process.env.ADMIN_CODE){
        newUser.isAdmin =true;
    }
    User.register(newUser,req.body.password,function(err,user){
        if(err){
            req.flash("error", err.message);
            res.redirect("back");
        }
        passport.authenticate("local")(req,res , function(){
            req.flash("success", "Welcome to Unreal Estate "+ user.username);
            res.redirect("/");
        });
    });
});

//LOG OUT LOGIC ROUTE

router.get("/logout",function(req,res){
    req.logout();
    req.flash("success", "Logged you out!!");
    res.redirect("/");
});

//FORGOT PASSWORD ROUTE

router.get("/forgot",function(req, res){
    res.render("forgot");
});

//FORGOT PASSWORD LOGIC ROUTE

router.post("/forgot", function(req, res, next){
    async.waterfall([
        function(done){
            crypto.randomBytes(20, function(err, buf){
                var token= buf.toString('hex');
                done(err, token);
            });
        },
        function(token, done){
            User.findOne({email: req.body.email}, function(err, user){
                if(!user){
                    req.flash("error", "No account with that email address exists");
                    return res.redirect("/forgot");
                }
                user.resetPasswordToken = token;
                user.resetPasswordExpires = Date.now() + 360000; //1 hour
                
                user.save(function(err){
                    done(err, token, user);
                });
            });
        },function(token, user, done){
            var smtpTransport = nodemailer.createTransport({
                service: 'Gmail',
                auth:{
                    user: 'gougarasv@gmail.com',
                    pass: process.env.GMAILPASS,
                }
            });
            var mailOptions = {
                to: user.email,
                from: 'gougarasv@gmail.com',
                subject: 'Unreal Estate Password Reset',
                text: 'You are receiving this because you (or someone else) have requested the reset of your password for the Unreal Estate web application.'+ '\n\n' +
                      'Please click on the following link, or paste this into your browser to complete the password change process.' + '\n\n' +
                      'http://' + req.headers.host + '/reset/' + token + '\n\n' +
                      'If you did not request this, please ignore this e-mail or change your password to ensure that no one has gained access to your account.'
            };
            smtpTransport.sendMail(mailOptions,function(err){
                console.log("mail sent");
                req.flash("success", "An e-mail has been sent to " + user.email + " with further instructions.")
                done(err,"done");
            });
        }
    ], function(err){
        if (err) return next(err);
        res.redirect("/forgot");
    });
});

//RESET PASSWORD ROUTE 

router.get('/reset/:token', function(req, res) {
    User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
      if (!user) {
        req.flash('error', 'Password reset token is invalid or has expired.');
        return res.redirect('/forgot');
      }
      res.render('reset', {token: req.params.token});
    });
  });
  
  router.post('/reset/:token', function(req, res) {
    async.waterfall([
      function(done) {
        User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
          if (!user) {
            req.flash('error', 'Password reset token is invalid or has expired.');
            return res.redirect('back');
          }
          if(req.body.password === req.body.confirm) {
            user.setPassword(req.body.password, function(err) {
              user.resetPasswordToken = undefined;
              user.resetPasswordExpires = undefined;
  
              user.save(function(err) {
                req.logIn(user, function(err) {
                  done(err, user);
                });
              });
            })
          } else {
              req.flash("error", "Passwords do not match.");
              return res.redirect('back');
          }
        });
      },
      function(user, done) {
        var smtpTransport = nodemailer.createTransport({
          service: 'Gmail', 
          auth: {
            user: 'gougarasv@gmail.com',
            pass: process.env.GMAILPASS
          }
        });
        var mailOptions = {
          to: user.email,
          from: 'gougarasv@gmail.com',
          subject: 'Your password has been changed',
          text: 'Hello,\n\n' +
            'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
        };
        smtpTransport.sendMail(mailOptions, function(err) {
          req.flash('success', 'Success! Your password has been changed.');
          done(err);
        });
      }
    ], function(err) {
      res.redirect('/');
    });
  });

module.exports = router;