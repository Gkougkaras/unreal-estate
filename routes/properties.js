const   express         = require("express"),
        router          = express.Router(),
        Property        = require("../models/property"),
        middleware      = require("../middleware"),
        NodeGeocoder    = require('node-geocoder');
 
const options = {
  provider: 'google',
  httpAdapter: 'https',
  apiKey: process.env.GEOCODER_API_KEY,
  formatter: null
};
 
const geocoder = NodeGeocoder(options);


//INDEX ROUTE

router.get("/",function(req, res){
    if(req.query.search){
        const regex = new RegExp(middleware.regex(req.query.search), "gi");
        Property.find({$or: [{location: regex}, {size:regex}, {price:regex}]},function(err, foundProperty){
            if (err){
                res.flash("error", "Something went wrong please try again");
                return res.redirect('back');
            }else{
                res.render("properties/index", {properties: foundProperty});
            }
        });
    }else{
    Property.find({},function(err, foundProperty){
        if (err){
            res.flash("error", "Something went wrong please try again");
            return res.redirect('back');
        }else{
            res.render("properties/index", {properties: foundProperty});
        }
            });
        };
    });


//POST ROUTE

router.post("/", middleware.isLoggedIn, function(req, res){
    const description = req.body.description;
    const bedroom    = req.body.bedroom;
    const image       = req.body.image;
    const price       = req.body.price;
    const size        = req.body.size;
    const owner = {
        id:req.user._id,
        username:req.user.username
    }
    geocoder.geocode(req.body.location, function (err, data) {
        if (err || !data.length) {
          req.flash('error', 'Invalid address');
          return res.redirect('back');
        }
        const lat = data[0].latitude;
        const lng = data[0].longitude;
        const location = data[0].formattedAddress;
    const newProperty = {location:location, lat: lat, lng: lng, image:image, description:description,price: price, owner: owner,size:size, bedroom:bedroom};
    Property.create(newProperty,function(err, newlyCreated){
        if(err){
            res.flash("error", "Sorry we could not create the ad, please try again");
            return res.redirect('back');
        }else{
            res.redirect("/properties");
        }
        });
    });
});

//NEW ROUTE

router.get("/new", middleware.isLoggedIn, function(req, res){
    res.render("properties/new");
});

//SHOW ROUTE

router.get("/:id", function(req, res){
    Property.findById(req.params.id, function(err, foundProperty){
        if(err || !foundProperty){
            req.flash("error", "Could not load Properties please try again");
            return res.redirect('/properties');
        }else{
            res.render("properties/show", {property: foundProperty})
        }
    });
});

//EDIT ROUTE
router.get("/:id/edit",middleware.checkPropertyOwnership,function(req, res){
    Property.findById(req.params.id, function(err,foundProperty){
         res.render("properties/edit", {property: foundProperty});
    });
});

//UPDATE ROUTE
router.put("/:id",middleware.checkPropertyOwnership,function(req,res){
    geocoder.geocode(req.body.location, function (err, data) {
        if (err || !data.length) {
          req.flash('error', 'Invalid address');
          return res.redirect('back');
        }
        req.body.property.lat = data[0].latitude;
        req.body.property.lng = data[0].longitude;
        req.body.property.location = data[0].formattedAddress;
    
    Property.findByIdAndUpdate(req.params.id,req.body.property,function(err,updatedProperty){
        if (err){
            req.flash("error", "Could not update the proprty please try again")
            res.redirect("/properties");
        }else{
            res.redirect("/properties/"+ req.params.id);
        }
    });
    });
});

//DESTROY ROUTE

router.delete("/:id", middleware.checkPropertyOwnership, function(req, res){
    Property.findByIdAndRemove(req.params.id, function(err){
        if (err){
            req.flash("error", "Could not delete the proprty please try again")
            res.redirect("/properties");
        }else{
            res.redirect("/properties");
        }
    });
});


module.exports = router;