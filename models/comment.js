const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/unreal_estate', { useNewUrlParser: true, useUnifiedTopology: true }); 

const commentSchema =mongoose.Schema({
    text:String,
    createdAt: { type: Date, default: Date.now },
    owner: {
        id:{type:mongoose.Schema.Types.ObjectId,
        ref:"User"
    },
        username: String
    }
})

module.exports = mongoose.model("Comment", commentSchema);