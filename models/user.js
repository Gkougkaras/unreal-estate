const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/unreal_estate', { useNewUrlParser: true, useUnifiedTopology: true }); 
const passportLocalMongoose =require("passport-local-mongoose");

const   UserSchema = new mongoose.Schema({
        firstName:  String,
        lastName:   String,
        avatar:     String,
        phone:      String,
        address:    String,
        city:       String,
        username:   String,
        password:   String,
        email:      {type:String, unique: true, required:true},
        resetPasswordToken: String,
        resetPasswordExpires: Date,
        isAdmin: {type:Boolean, default:false}
});

UserSchema.plugin(passportLocalMongoose);
module.exports =mongoose.model("User", UserSchema);