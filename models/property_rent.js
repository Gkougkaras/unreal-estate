const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/unreal_estate', { useNewUrlParser: true, useUnifiedTopology: true }); 

const   propertyRentSchema = new mongoose.Schema({
        image: String,
        price: String,
        size:String,
        bedroom:String,
        description: String,
        location:String,
        lat: Number,
        lng: Number,
        createdAt: { type: Date, default: Date.now },
        owner:{
            id:{
                type: mongoose.Schema.Types.ObjectId,
                ref: "User"
            },
            username: String
        },
        comments:[
            {
                type: mongoose.Schema.Types.ObjectId,
                ref:"Comment"
            }
        ]
});

module.exports = mongoose.model("PropertyRent", propertyRentSchema);

