const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/unreal_estate', { useNewUrlParser: true, useUnifiedTopology: true }); 

const   landSchema = new mongoose.Schema({
        image: String,
        price: String,
        size:String,
        description: String,
        location:String,
        lat: Number,
        lng: Number,
        createdAt: { type: Date, default: Date.now },
        owner:{
            id:{
                type: mongoose.Schema.Types.ObjectId,
                ref: "User"
            },
            username: String
        }
    });

module.exports = mongoose.model("Land", landSchema);

