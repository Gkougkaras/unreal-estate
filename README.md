This is my first full-stack application.

It is deployed at this URL: https://unreal-estate.herokuapp.com/

The stack that i use is HTML5, CSS3, Bootstrap and Javascript for the front end.
Node.JS with Express for the backend and MongoDB as the Database.

The app consists of one home landing page that is accesible to everyone for Read only purposes.
The drop down for bying properties is also read only and anyone has access to that so that they can browse the posted properties.

The about us, careers and contact pages are also there for better user experience.

For the rest of the content a user has to sign up. 
When someone signs up through the form they have the ability to post a new property, property for rent or Land for sale.
The Ad persists at the database(MongoDB Atlas).
A private user profile is created on sign up that only the user and not even an admin has access to it.
It is made for reference to the ads each user has posted.
A User has the ability to remove or edit one of his ads and also add a comment on the ad of another user at the property rent page.
An admin has no access to the private user profiles but has the ability to delete or edit any users content from the show page.
To become an admin you need to pass a secret code on the sign up form.

There is a fuzzy search option added at the show page that finds properties by location,size or price.
A contact form where a user can use his email to sent a comment with send grid is added at the footer.

There will be more soon.




