const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/unreal_estate', { useNewUrlParser: true, useUnifiedTopology: true }); 

const Property = require("./models/property");
const PropertyRent = require("./models/property_rent");
const Land = require ("./models/land");
const Comment =require ("./models/comment");

const seeds =[
    {image:"https://images.unsplash.com/photo-1580587771525-78b9dba3b914?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
     price:"10000000",
     size:"250",
     bedroom:"7",
     description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."},
    {image:"https://images.unsplash.com/photo-1512917774080-9991f1c4c750?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
    price:"12000000",
    size:"550",
    bedroom:"10",
    description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."},
    {image:"https://images.unsplash.com/flagged/photo-1563891615762-0f4dfc63cc14?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
    price:"40000000",
    size:"1250",
    bedroom:"27",
    description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."},
    {image:"https://images.unsplash.com/photo-1580587771525-78b9dba3b914?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
     price:"10000000",
     size:"250",
     bedroom:"7",
     description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."}
]
//REMOVE ALL PROPERTIES
async function seedDB(){
    try{
        await Property.remove({});
        console.log("Properties Removed");
        await PropertyRent.remove({});
        console.log("Properties for Rent Removed");
        await Land.remove({});
        console.log("Lands Removed");
        await Comment.remove({});
        console.log("Comments Removed");
        
        for( const seed of seeds){
            let propertyRent =await PropertyRent.create(seed);
            console.log("Property Rent created");
            let comment =await Comment.create({
                text: "What a nice Villa",
                owner: "Bill"
                });
                console.log("Comment created");
            let property =await Property.create(seed);
            console.log("Property created");
            let land =await Land.create(seed);
            console.log("Land created");
            propertyRent.comments.push(comment);
            console.log("Comments added to Property for Rent");
            property.save();
            propertyRent.save();
            land.save();
        }
    }catch(err){
        console.log(err);
    }
}
    
   
module.exports = seedDB;