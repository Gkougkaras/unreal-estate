if (process.env.NODE_ENV !== 'production') require('dotenv').config()

const    express            = require("express"),
         app                = express(),
         bodyParser         = require("body-parser"),
         mongoose           = require("mongoose"),
         flash              = require("connect-flash"),
         passport           = require("passport"),
         LocalStrategy      = require("passport-local"),
         methodOverride     = require("method-override"),
         Property           = require("./models/property"),
         PropertyRent       = require("./models/property_rent"),
         Land               = require("./models/land"),
         Comment            = require("./models/comment"),
         User               = require("./models/user");
        //  seedDB = require("./seeds");

        
         //REQUIRING ROUTES
        const   propertyRentRoutes  = require("./routes/properties_rent"),
                propertyRoutes      = require("./routes/properties"),
                landRoutes          = require("./routes/lands"),
                commentRoutes       = require("./routes/comments"),
                userRoutes          = require("./routes/user"),
                indexRoutes         = require("./routes/index");
                
        const mongoPass=process.env.MONGOPASS;
        // 
          //LOCAL DB mongoose.connect('mongodb://localhost:27017/unreal_estate', { useNewUrlParser: true, useUnifiedTopology: true });
         mongoose.connect('mongodb+srv://VasileiosGkougkaras:'+mongoPass+'@unrealestate-rr5kp.mongodb.net/test?retryWrites=true&w=majority',
        { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex:true }).then(()=>{
            console.log("Connected to DB");;
        }).catch(err =>{
            console.log("ERROR", err.message);
        });
         app.use(express.json());
         app.use(express.urlencoded({extended: true}));
         app.set("view engine", "ejs");
         app.use(express.static(__dirname +"/public"));
         app.use(methodOverride("_method"));
         app.use(flash());
        //  seedDB();
         app.use(require("express-session")({
            secret:"Lorem Ipsum",
            resave: false,
            saveUninitialized: false
        }));

        app.locals.moment = require('moment');
        app.use(passport.initialize());
        app.use(passport.session());
        passport.use(new LocalStrategy(User.authenticate()));
        passport.serializeUser(User.serializeUser());
        passport.deserializeUser(User.deserializeUser());

        app.use(function(req,res,next){
            res.locals.currentUser=req.user;
            res.locals.error=req.flash("error");
            res.locals.success=req.flash("success");
            next();
        });

        

        app.use("/properties_rent",propertyRentRoutes),
        app.use("/properties",propertyRoutes),
        app.use("/properties_rent/:id/comments",commentRoutes),
        app.use(indexRoutes),
        app.use("/users",userRoutes),
        app.use("/lands",landRoutes);

        let port = process.env.PORT;
        if (port == null || port == "") {
          port = 3000;
        }
        app.listen(port);
        