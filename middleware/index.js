//ALL MIDDLEWARES

const   Property          = require("../models/property"),
        PropertyRent      = require("../models/property_rent"),
        Land              = require("../models/land"),
        Comment           = require("../models/comment"),
        middlewareObject  = {};

middlewareObject.checkPropertyOwnership = function(req, res, next){
    if (req.isAuthenticated()){
        Property.findById(req.params.id, function(err,foundProperty){
            if (err || !foundProperty){
                req.flash("error", "Property not found");
                res.redirect("/properties");
            }
            else if(foundProperty.owner.id.equals(req.user._id) || req.user.isAdmin){
                req.property=foundProperty;
                next();
            }
            else
            {
                req.flash("error", "You don't have permission to do that");
                res.redirect("/properties/" + req.params.id);
            }
        });
    }else{
        req.flash("error", "You need to be logged in to do that");
        res.redirect("back");
    }
}

middlewareObject.checkPropertyRentOwnership = function(req, res, next){
    if (req.isAuthenticated()){
        PropertyRent.findById(req.params.id, function(err,foundPropertyRent){
            if (err || !foundPropertyRent){
                req.flash("error", "Property not found");
                res.redirect("/properties_rent");
            }
            else if(foundPropertyRent.owner.id.equals(req.user._id)|| req.user.isAdmin ){
                    req.propertyrent=foundPropertyRent;
                    next();
            }
            else{
                    req.flash("error", "You don't have permission to do that");
                    res.redirect("back");
            }
        });
    }else{
        req.flash("error", "You need to be logged in to do that");
        res.redirect("back");
    }
}

middlewareObject.checkLandOwnership = function(req, res, next){
    if (req.isAuthenticated()){
        Land.findById(req.params.id, function(err,foundLand){
            if (err || !foundLand){
                req.flash("error", "Property not found");
                res.redirect("/lands");
            }
            else if(foundLand.owner.id.equals(req.user._id) || req.user.isAdmin ){
                    req.land=foundLand;
                    next();
                }else{
                    req.flash("error", "You don't have permission to do that");
                    res.redirect("back");
                }
        });
    }else{
        req.flash("error", "You need to be logged in to do that");
        res.redirect("back");
    }
}

middlewareObject.checkCommentOwnership = function(req, res, next){
    if (req.isAuthenticated()){
        Comment.findById(req.params.comment_id, function(err,foundComment){
            if (err){
                res.redirect("back");
            }
            else{
                if(foundComment.owner.id.equals(req.user._id) || req.user.isAdmin){
                    next();
                }else{
                    req.flash("error", "You don't have permission to do that");
                    res.redirect("back");
                }
            }
        });
    }else{
        req.flash("error", "You need to be logged in to do that");
        res.redirect("back");
    }
}

middlewareObject.isLoggedIn =function(req,res ,next){
    if(req.isAuthenticated()){
        return next();
    }
    req.flash("error", "You need to be logged in to do that");
    res.redirect("/login");
}

middlewareObject.regex = function escapeRegex(text){
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g,"\\$&");
};



module.exports= middlewareObject;